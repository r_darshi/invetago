package com.example.user.investago;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.Image;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

public class StartupProfile extends AppCompatActivity {
    ImageView profile;
    TextView name, company, title, worth, email;

    //SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup_profile);
        profile = (ImageView) findViewById(R.id.profile_image);
        name = (TextView) findViewById(R.id.name);
        company = (TextView) findViewById(R.id.comp);
        title = (TextView) findViewById(R.id.tit);
        worth = (TextView) findViewById(R.id.worth1);
        email = (TextView) findViewById(R.id.email);

        Glide.with(StartupProfile.this).load(null).into(profile);
        name.setText("");
        company.setText("");
        title.setText("");
        worth.setText("");
        email.setText("");

//        sharedPreferences= PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String s = getIntent().getStringExtra("Details");
        String to[] = s.split(":");

        String Email = to[1];
        String Company = to[0];
        String Title = to[3];
        String Worth = to[4];
        String Name = to[2];
        String Uri = to[5];
        Log.i("CkeckS",s);
        Log.i("CkeckS",Name+" "+Company+" "+Title+" "+Worth+" "+Email+" "+Uri);

       // Toast.makeText(this,s,Toast.LENGTH_LONG).show();

        if (!Uri.isEmpty() || Uri.length() != 0)
            Glide.with(StartupProfile.this).load(Uri).into(profile);
        name.setText(Name);
        company.setText(Company);
        title.setText(Title);
        worth.setText(Worth);
        email.setText(Email);

    }

    public void review(View v){

        final AlertDialog.Builder customDialog
                = new AlertDialog.Builder(StartupProfile.this);
        customDialog.setTitle("Suggestions");

        LayoutInflater layoutInflater
                = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=layoutInflater.inflate(R.layout.comment,null);
        customDialog.setView(view);
        customDialog.show();

        Button post=(Button)view.findViewById(R.id.post);
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                customDialog.setCancelable(true);
                Toast.makeText(StartupProfile.this,"Posted",Toast.LENGTH_SHORT).show();
            }
        });


    }
}
