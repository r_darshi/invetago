package com.example.user.investago;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class InvesterHome extends AppCompatActivity {

    private ListView listView;
    private FloatingActionButton filter;
    private ArrayList<StartUpDetails> startups = new ArrayList<>();
    StartupAdapter adapter;
    DatabaseReference mdatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.investor_home);


        ////////hiding status bar///////
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //////////////////

        /////////////Collapsing Tool bar Setup////////////////
        final CollapsingToolbarLayout collapsingToolbarLayout =
                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(" ");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);

        mdatabase = FirebaseDatabase.getInstance().getReference().child("startup");

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle("InvestaGo");
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");
                    isShow = false;
                }
            }
        });
////////////////////////////////////////////////////////

        listView = (ListView) findViewById(R.id.list_startup);
        listView.setNestedScrollingEnabled(true);
        filter = (FloatingActionButton) findViewById(R.id.filter);
        adapter = new StartupAdapter(this, R.layout.startup_item, startups);
        listView.setAdapter(adapter);

        ////////////////delete this part//////////
        mdatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                StartUpDetails det = dataSnapshot.getValue(StartUpDetails.class);
                Log.i("datasnapshot value", String.valueOf(dataSnapshot.getValue()));
                Log.i("details", String.valueOf(det));
                startups.add(det);
                Log.i("arraylist", String.valueOf(startups));
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

/*

        startups.add(new StartUpDetails("SkateBoard park", "Rakhi", "Rakhi with Nishijeet", "40000", "asd@asd", "pass"));
        startups.add(new StartUpDetails("Haunted House", "Rakhi", "Rakhi walli Company", "90000", "asd@asd", "pass"));
        /////////////////////////////////////////
*/


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                StartUpDetails st = startups.get(position);
                String passable = st.getCompany() + ":" + st.getEMail() + ":" + st.getName() + ":" + st.getTitle() + ":" + st.getUri() + ":" + st.getWorth();
                Toast.makeText(InvesterHome.this, passable, Toast.LENGTH_LONG).show();
                Intent i = new Intent(InvesterHome.this, StartupProfile.class);
                i.putExtra("Details", passable);
                startActivity(i);

            }
        });

    }


    public void open_profile(View v) {
        startActivity(new Intent(InvesterHome.this, InvestorProfile.class));
    }
}