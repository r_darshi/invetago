package com.example.user.investago;

import android.net.Uri;

/**
 * Created by Rakhi on 28-09-2017.
 */
public class StartUpDetails {
    String Name, Company, Worth, EMail, Pass, title,uri;


    public StartUpDetails() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getWorth() {
        return Worth;
    }

    public void setWorth(String worth) {
        Worth = worth;
    }

    public String getEMail() {
        return EMail;
    }

    public void setEMail(String EMail) {
        this.EMail = EMail;
    }

    public String getPass() {
        return Pass;
    }

    public void setPass(String pass) {
        Pass = pass;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
